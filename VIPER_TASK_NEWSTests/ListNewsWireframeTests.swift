//
//  ListNewsWireframeTests.swift
//  VIPER_TASK_NEWSTests
//
//  Created by Nguyen Minh Tri on 12/21/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import XCTest
@testable import VIPER_TASK_NEWS

class ListNewsWireframeTests: XCTestCase {
    
    var listNewsWireframe: ListNewsWireframe?
    
    override func setUp() {
        super.setUp()
        listNewsWireframe = ListNewsWireframe()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testListNewsScreen(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let navController = storyboard.instantiateViewController(withIdentifier: "ListNewsNavigationController")
        let view = navController.childViewControllers.first as? ListNewsView
        
        XCTAssertNotNil(view, "ERROR IN DOING LOAD SCREEN LIST NEWS")

    }
    
    func testShowDetailNews(){
        
        
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
