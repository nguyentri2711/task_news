//
//  DetailNewsWireframe.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/13/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import Foundation
import UIKit

class DetailNewsWireframe{
    
    func getCreateModule(news: NewsModel) -> UIViewController{
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let view = storyboard.instantiateViewController(withIdentifier: "DetailNewsViewController") as! DetailNewsView
        
        let presenter = DetailNewsPresenter()
        let interactor = DetailNewsInteractor(news: news)
        
        presenter.detailNewsView = view
        view.detailNewsPresenter = presenter
        
        presenter.detailNewsInteractor = interactor
        interactor.detailNewsPresenter = presenter
        
        return view
        
    }
    
}
